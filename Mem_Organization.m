% Doing scheduling for the input matrix
clc

load LDPCCodeData_Rate_0_83_Code_12_72_Sc_8_C12.mat
LDPCCodeData
H_Base = LDPCCodeData.H_Base;
H_Shift = LDPCCodeData.H_Shift;
H_rows = size(H_Shift, 1);
H_cols = size(H_Shift, 2);
memories = 6
cols_per_mem = H_cols/memories
dc = sum(H_Base(1,:)~=0);    % degree of checknode
dv = sum(H_Base(:,1)~=0);    % degree of var node
mem_y1 = zeros(H_rows,cols_per_mem); mem_y2 = zeros(H_rows,cols_per_mem); 
mem_y3 = zeros(H_rows,cols_per_mem); mem_y4 = zeros(H_rows,cols_per_mem);
mem_y5 = zeros(H_rows,cols_per_mem); mem_y6 = zeros(H_rows,cols_per_mem);

k = 1; m = 1;
row = 1;
while row < H_rows
  %%%%%%%%%%%%%%%%%%%%%%%%%%% populating H_Base over memories %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for i = 1 : H_rows

    for j = 0 : H_cols-1
            if(j<cols_per_mem)
            mem_y1(i,1+ mod(j,cols_per_mem)) = H_Base(i, j+1);
            elseif (j>=cols_per_mem &&   j< cols_per_mem*2)
            mem_y2(i,1+mod(j,cols_per_mem)) = H_Base(i, j+1); 
            elseif (j>=cols_per_mem*2 && j< cols_per_mem*3)
            mem_y3(i,1+mod(j,cols_per_mem)) = H_Base(i, j+1); 
            elseif (j>=cols_per_mem*3 && j< cols_per_mem*4)
            mem_y4(i,1+mod(j,cols_per_mem)) = H_Base(i, j+1);
            elseif (j>=cols_per_mem*4 && j< cols_per_mem*5)
            mem_y5(i,1+mod(j,cols_per_mem)) = H_Base(i, j+1);
            else
            mem_y6(i,1+mod(j,cols_per_mem)) = H_Base(i, j+1);
            end
    end
  end

 
  %%%%%%%%%%%%%%%%%%%%%%%% Determine the extra cycles %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  d = [ 0 0 0 0 0 0];
  extra_cycles = 0;
         s = 1;
         for s= 1: H_rows
            d (1,1) = sum(mem_y1(s,:)~=0);    % degree of checknode
            d (1,2) = sum(mem_y2(s,:)~=0);    % degree of checknode
            d (1,3) = sum(mem_y3(s,:)~=0);    % degree of checknode
            d (1,4) = sum(mem_y4(s,:)~=0);    % degree of checknode
            d (1,5) = sum(mem_y5(s,:)~=0);    % degree of checknode
            d (1,6) = sum(mem_y6(s,:)~=0);    % degree of checknode
            extra_cycles = length(find(d(1,:) == 5)) + length(find(d(1,:) == 6))*2 +  length(find(d(1,:) == 7))*3 + length(find(d(1,:) == 8))*4 + extra_cycles;                 
         end
sprintf('Total extra cycles: %d', extra_cycles)
  
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
nz_m = [ 0 0 0 0 0 0];
dc_m = [ 0 0 0 0 0 0]; %degree of checknode of six memories

    dc_m (1,1) = sum(mem_y1(row,:)~=0);    % degree of checknode
    dc_m (1,2) = sum(mem_y2(row,:)~=0);    % degree of checknode
    dc_m (1,3) = sum(mem_y3(row,:)~=0);    % degree of checknode
    dc_m (1,4) = sum(mem_y4(row,:)~=0);    % degree of checknode
    dc_m (1,5) = sum(mem_y5(row,:)~=0);    % degree of checknode
    dc_m (1,6) = sum(mem_y6(row,:)~=0);    % degree of checknode
 
    
    nz_m     = find(dc_m(1,:) ~= 4);    % un-balanced memories
    pair_unb_mems = floor(length(nz_m)/2)      % pair of un-balanced memories
    
    if(sum(nz_m)>0)
     t = 0
      if(dc_m(nz_m(1)) == dc_m(nz_m(2)))
        nz_m = circshift(nz_m,[0,-1])
      end
     while t < pair_unb_mems   %determine two columns in two memories that need a swap
        p = nz_m(1)
        dc_p = sum(H_Base(row,1+(p-1)*H_rows:p*H_rows))
        q = nz_m(2)
        dc_q = sum(H_Base(row,1+(q-1)*H_rows:q*H_rows))
        if(dc_p<4)
            c1 = find(H_Base(row,1+(p-1)*H_rows:p*H_rows) ~= 1) + ((p-1)*H_rows)
        else
            c1 = find(H_Base(row,1+(p-1)*H_rows:p*H_rows) ~= 0) + ((p-1)*H_rows)
        end
    
        if(dc_q<4)
            c2 = find(H_Base(row,1+(q-1)*H_rows:q*H_rows) ~= 1) + ((q-1)*H_rows)
        else
            c2 = find(H_Base(row,1+(q-1)*H_rows:q*H_rows) ~= 0) + ((q-1)*H_rows)
        end
        
        if(k == 0)
            k = 1 
        end
        if(m == 0)
            m = 1 
        end
       
        H_Base(:,[c1(:,k) c2(:,m)]) = H_Base(:,[c2(:,m) c1(:,k)]); % swap kth element of c1 with mth element of c2
        k = mod(k+1,length(c1))
        m = mod(m+1,length(c2))
        nz_m(1) =  []; nz_m(1)= []
        t = t + 1
     end
       
    else
         row = row + 1
         
    
    end
    
    end
                
  

             
                

 
    
    
    
    
    
    

%p = b(k, 1);
%q = b(k, 2);
% H_Base(:,[p q]) = H_Base(:,[q p]) % swap p and q columns in H_Base
 
%end   
    
mem_y1

%function swap(x,y,p,q) %swap col p in matrix x with col q in matrix y
%[x(:,p),y(:,q)] = deal(y(:,q),x(:,p)); 
%end
    %nz_circulants_row_i     = find(H_Shift(i,:) ~= -1);
    %num_nz_circulants_row_i = length(nz_circulants_row_i);

